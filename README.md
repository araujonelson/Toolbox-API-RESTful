Toolbox assessment #2.

Node.js API RESTful.

Dependencies used for production:

Express.js
body-parser as a middleware for Express.js
Mongoose
Morgan

Dev dependencies (For unit testing)
chai
chai-http
mocha

Endpoints:
POST /products/seed (Seeds the database with 3 sample products)
GET  /products/all  (Retrieves a list with all products)
GET  /products/:id  (Retrieves a specific product which matches the given ID)
PUT  /products/:id/:quantity (Adds the given qunatity from stock field)
DELETE  /products/:id/:quantity (Removes the given qunatity from stock field)

Unit Testing:
npm test (it has a 10 seconds timeout since the database is hosted in the cloud).

Default port is 3001.