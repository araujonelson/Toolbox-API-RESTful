let Products = require('../models/product.js');

module.exports = function(app) {

    app.get('/products/all', (req, res) => {
        Products.find({}, function(err, products) {
            if(err) {
                throw err;
            } else if(products.length == 0) {
                res.json({"status": "failed", "message": "nothing found yet :)"});
            }
            else {
                res.send(products)
            }
        });
    });

    app.get('/products/:id', (req, res) => {
        Products.findById({ _id: req.params.id }, function(err, products) {
            if(err) {
                res.status(400);
                res.json({"status": "failed", "message": err});
            } else {
                res.send(products)
            }
        });
    });
    
    app.put('/products/:id/:quantity', (req, res) => {
        Products.findOneAndUpdate({ _id: req.params.id }, { $inc: { quantity: +req.params.quantity } }, {new: true}, function(err, products) {
            if(err) {
                res.status(400);
                res.json({"status": "failed", "message": err});
            } else {
                res.send(products)
            }
        });
    });

    app.delete('/products/:id/:quantity', (req, res) => {
        Products.findOneAndUpdate({ _id: req.params.id }, { $inc: { quantity: -req.params.quantity } }, {new: true}, function(err, products) {
            if(err) {
                res.status(400);
                res.json({"status": "failed", "message": err});
            } else {
                res.send(products)
            }
        });
    });
}