let Product = require('../models/product.js');

module.exports = function(app) {

    app.post('/products/seed', (req, res) => {
        //Seed the database
        var sampleProducts = [
            {name: "Xbox One", quantity:"200", price: "20000"},
            {name: "Playstation 4", quantity:"100", price: "23000"},
            {name: "Wii U", quantity:"50", price: "17000"}];

        Product.collection.insert(sampleProducts, (err, msg) => {
            if(err){
                res.status(204);
                res.json({"status": "failed", "message": err});
            } else {
                var message = 'Seed sucessful. All products were inserted.';
                res.json({"status": "success", "message": message});
            }
        });
    });
}