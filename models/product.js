let mongoose = require('mongoose');

//Instance the Schema
let Schema = mongoose.Schema;

//Create the 'Product' Schema we are going to use for this app.
let productSchema = new Schema({
	name: String,
	quantity: Number,
	price: String
});

let Product = mongoose.model('Product', productSchema);

module.exports = Product;