//Express as Web Framework.
let express = require('express');
let app = express();

var morgan = require('morgan')
//app.use(morgan('combined'))

//MongoDB as Non-relational SQL.
let mongoose = require ('mongoose');
let config = require('./config');

let seedController = require('./controllers/seedController');
let apiController = require('./controllers/apiController');

//Connect the database
mongoose.connect(config);

/***********************************\
*         Seed the database         *
* GET: /products/seed               *
\***********************************/
seedController(app);

/***********************************\
*          Main Controller          *
* GET: /products/all                *
* GET: /products/:id                *
* PUT: /products/:id/:quantity      *
* DELETE: /products/:id/:quantity   *
\***********************************/
apiController(app);

//Setup server.
app.listen(3001, () => {
	console.log("Server has been initialized on port 3000");
});

module.exports = app; //For testing purposes.