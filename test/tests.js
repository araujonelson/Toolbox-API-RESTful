let mongoose = require("mongoose");
let Product = require('../models/product.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);
describe('Products', () => {
    beforeEach((done) => { //Before each test we empty the database
        Product.remove({}, (err) => { 
           done();         
        });     
    });
  /*
  * Test the /GET route
  */
  describe('/GET Products', () => {
      it('it should return an error when trying to get all products and it\'s empty', (done) => {
        chai.request(server)
            .get('/products/all')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('status');
                res.body.status.should.equal('failed');
              done();
            });
      });
  });
  /*
  * Test the /POST route
  */
  describe('/POST Products', () => {
      it('it should seed the database with 3 sample products', (done) => {
        chai.request(server)
            .post('/products/seed')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('status');
                res.body.status.should.equal('success');
              done();
            });
      });
  });
  /*
  * Test the /GET route
  */
  describe('/GET Products', () => {
      it('it should return an error when passing a wrong id', (done) => {
        chai.request(server)
            .get('/products/1')
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.have.property('status');
                res.body.status.should.equal('failed');
              done();
            });
      });
  });
});